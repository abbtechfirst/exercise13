package org.example;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class UserServiceTest {
    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testIsUserActive_UserIsActive() {
        String username = "activeUser";
        User user = new User();
        user.setActive(true);
        when(userRepository.findByUsername(username)).thenReturn(user);
        boolean result = userService.isUserActive(username);
        assertTrue(result);
        verify(userRepository, times(1)).findByUsername(username);
    }

    @Test
    public void testIsUserActive_UserIsInactive() {
        String username = "inactiveUser";
        User user = new User();
        user.setActive(false);
        when(userRepository.findByUsername(username)).thenReturn(user);
        boolean result = userService.isUserActive(username);
        assertFalse(result);
        verify(userRepository, times(1)).findByUsername(username);
    }

    @Test
    public void testIsUserActive_UserNotFound() {
        String username = "nonExistentUser";
        when(userRepository.findByUsername(username)).thenReturn(null);
        boolean result = userService.isUserActive(username);
        assertFalse(result);
        verify(userRepository, times(1)).findByUsername(username);
    }

    @Test(expected = Exception.class)
    public void testDeleteUser_UserNotFound() throws Exception {
        Long userId = 1L;
        when(userRepository.findUserId(userId)).thenReturn(null);
        userService.deleteUser(userId);
        verify(userRepository, times(1)).findUserId(userId);
    }

    @Test
    public void testDeleteUser_UserExists() throws Exception {
        Long userId = 1L;
        User user = new User();
        when(userRepository.findUserId(userId)).thenReturn(user);
        userService.deleteUser(userId);
        verify(userRepository, times(1)).findUserId(userId);
    }

    @Test(expected = Exception.class)
    public void testGetUser_UserNotFound() throws Exception {
        Long userId = 1L;
        when(userRepository.findUserId(userId)).thenReturn(null);
        userService.getUser(userId);
        verify(userRepository, times(1)).findUserId(userId);
    }

    @Test
    public void testGetUser_UserExists() throws Exception {
        Long userId = 1L;
        User user = new User();
        when(userRepository.findUserId(userId)).thenReturn(user);
        User result = userService.getUser(userId);
        assertEquals(user, result);
        verify(userRepository, times(1)).findUserId(userId);
    }
}
